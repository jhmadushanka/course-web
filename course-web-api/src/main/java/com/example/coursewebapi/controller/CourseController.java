package com.example.coursewebapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.coursewebapi.model.Course;
import com.example.coursewebapi.repository.CourseRepository;

@RestController
@RequestMapping("/course")
public class CourseController {
	
	@Autowired
	CourseRepository courseRepository;
	
	@GetMapping(value = "/all")
	public List<Course> getAll() {
		return courseRepository.findAll();
	}
	
	@GetMapping(value = "/findById/{id}")
	public List<Course> getCourseById(@PathVariable final int id) {
		return courseRepository.findById(id);
	}
	
	@GetMapping(value = "/findByCourseCode/{courseCode}")
	public List<Course> getCourserByCourseCode(@PathVariable final String courseCode) {
		return courseRepository.findByCourseCode(courseCode);
	}
	
	@PostMapping("/course")
	public Course createCouse(@RequestBody Course course) {
		return courseRepository.save(course);
	}
	
	@PutMapping("/course/{id}")
	public Course replaceCourse(@RequestBody Course newCourse, @PathVariable Integer id) {

		return courseRepository.findById(id)
			.map(course -> {
				course.setCourseCode(newCourse.getCourseCode());
				course.setCourseName(newCourse.getCourseName());
				course.setCredit(newCourse.getCredit());
				return courseRepository.save(course);
			})
			.orElseGet(() -> {
				newCourse.setId(id);
				return courseRepository.save(newCourse);
			});
	}

	@DeleteMapping("/course/{id}")
	void deleteCourse(@PathVariable Integer id) {
		courseRepository.deleteById(id);
	}

}
