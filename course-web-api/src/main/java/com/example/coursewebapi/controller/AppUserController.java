package com.example.coursewebapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.coursewebapi.model.AppUser;
import com.example.coursewebapi.repository.AppUserRepository;;

@RestController
@RequestMapping("/app_user")
public class AppUserController {
	
	@Autowired
	AppUserRepository appUserRepository;
	
	@GetMapping(value = "/all")
	public List<AppUser> getAll() {
		return appUserRepository.findAll();
	}
	
	@GetMapping(value = "/findById/{id}")
	public List<AppUser> getUserById(@PathVariable final int id) {
		return appUserRepository.findById(id);
	}
	
	@GetMapping(value = "/findByIndex/{index}")
	public List<AppUser> getUserByUserName(@PathVariable final String userName) {
		return appUserRepository.findByUserName(userName);
	}
	
	@PostMapping("/app_user")
	public AppUser createUser(@RequestBody AppUser users) {
		return appUserRepository.save(users);
	}
	
	@PutMapping("/app_user/{id}")
	public AppUser replaceUser(@RequestBody AppUser newUser, @PathVariable Integer id) {

		return appUserRepository.findById(id)
			.map(user -> {
				user.setName(newUser.getName());
				user.setUserName(newUser.getUserName());
				user.setPassword(newUser.getPassword());
				user.setEmail(newUser.getEmail());
				user.setPhoneNumber(newUser.getPhoneNumber());
				user.setRole(newUser.getRole());
				return appUserRepository.save(user);
			})
			.orElseGet(() -> {
				newUser.setId(id);
				return appUserRepository.save(newUser);
			});
	}

	@DeleteMapping("/app_user/{id}")
	void deleteUser(@PathVariable Integer id) {
		appUserRepository.deleteById(id);
	}


}
