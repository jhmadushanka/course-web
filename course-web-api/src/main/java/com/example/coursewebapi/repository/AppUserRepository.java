package com.example.coursewebapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.coursewebapi.model.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Integer>{
	
	List<AppUser> findById(int id);
	
	List<AppUser> findByUserName(String userName);

}
