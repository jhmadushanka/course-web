package com.example.coursewebapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.coursewebapi.model.Course;

public interface CourseRepository extends JpaRepository<Course, Integer>{

	List<Course> findById(int id);
	
	List<Course> findByCourseCode(String courseCode);
	
	
}
